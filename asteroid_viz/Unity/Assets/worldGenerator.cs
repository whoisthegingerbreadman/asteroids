﻿using System.Collections;
using System.Collections.Generic;
using System.IO;
using System;
using UnityEngine;

public class worldGenerator : MonoBehaviour
{
    // Start is called before the first frame update
    public string run_number="1";
    public int speed=1;
    static string pattern ="history*";
    string[] dirs;
    GameObject[] spheres;
    string base_path = "../../AsteroidsMB/runs/run";
    int num_bodies=0;

    int T = 0;
    int t = 0;


    Vector3[][] histories;

    private Vector3 StringToVector3(string sVector)
     {
         // Remove the parentheses
         if (sVector.StartsWith ("(") && sVector.EndsWith (")")) {
             sVector = sVector.Substring(1, sVector.Length-2);
         }
        //Debug.Log(sVector);
         // split the items
         string[] sArray = sVector.Split(',');
 
         // store as a Vector3
        //  Debug.Log("blah");
         float x = float.Parse(sArray[0]);
         float y = float.Parse(sArray[1]);
         float z = float.Parse(sArray[2]);
         
         Vector3 result = new Vector3(x,y,z);
        // Debug.Log("blah2");
        //Debug.Log(x);
        // Debug.Log("blah3");
        //Debug.Log(y);
        // Debug.Log("blah4");
        //Debug.Log(z);
        // Debug.Log("blah5");
        return result;
     }
    Vector3[] getHistory(int i){
        //Debug.Log("Inside GetHistory ");
        string path = dirs[i];
        string fileData = System.IO.File.ReadAllText(path);
        string[] lines  = fileData.Split("\n"[0]);
        Vector3[] history = new Vector3[lines.Length]; 
        T = lines.Length-1;
        for(int j =0;j<T;j++){
            //Debug.Log("KLAG");
            Vector3 x = StringToVector3(lines[j]);
            //Debug.Log(x);
            history[j] = x;
        }
        //Debug.Log("Exiting GetHistory ");
        return history;

    }
    void Start()
    { 
        
        string path = base_path+run_number+"/";
        Debug.Log("Getting data from "+ path);
        dirs = Directory.GetFiles(path,pattern);
        /*foreach(string dir in dirs){
            Debug.Log(dir);
        }*/
        num_bodies = dirs.Length;
        histories = new Vector3[num_bodies][];
        spheres = new GameObject[num_bodies];
        Debug.Log(num_bodies);
        for(int i =0;i<num_bodies;i++){
            GameObject sphere = GameObject.CreatePrimitive(PrimitiveType.Sphere);
            spheres[i] = sphere;
            //Debug.Log("Getting History ");
            histories[i] = getHistory(i);

        }
        //Debug.Log(histories[0][0]);


        
    }

    // Update is called once per frame
    void Update()
    {
        int i = Math.Min(T,t);
        for(int j=0;j<num_bodies;j++){
            //Debug.Log(i);
            spheres[j].transform.position = histories[j][i];
        }

        t+=speed;
        
    }
}
