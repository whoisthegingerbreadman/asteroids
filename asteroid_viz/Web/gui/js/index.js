var scene = new THREE.Scene();

var camera = new THREE.PerspectiveCamera(75, window.innerWidth / window.innerHeight, 0.1, 1000);

var renderer = new THREE.WebGLRenderer();
var directionalLight = new THREE.DirectionalLight(0xffffff, 0.5);

scene.add(directionalLight);
renderer.setSize(window.innerWidth, window.innerHeight);
document.body.appendChild(renderer.domElement);

let ceres = asteroid(50, 0x888, ceresMap);
scene.add(ceres);

camera.position.z = 300;
var animate = function () {
  requestAnimationFrame(animate);

  ceres.rotation.x += 0.01;
  //ceres.rotation.y += 0.01;

  renderer.render(scene, camera);
};

animate();