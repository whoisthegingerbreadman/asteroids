import { SphereGeometry, TextureLoader, MeshPhysicalMaterial, Mesh } from "three";

export default {
  methods: {
    createSphereObject(radius, color, textureMap) {
      let geometry = new SphereGeometry(radius, 500, 500);
      let map = textureMap && new TextureLoader().load(textureMap);
      let material = new MeshPhysicalMaterial({
        color,
        reflectivity: 0.5,
        map
      });

      return new Mesh(geometry, material);
    }
  }
}