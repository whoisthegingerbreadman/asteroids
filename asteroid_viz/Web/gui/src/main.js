import Vue from 'vue'
import App from './App.vue'
import VueStatic from 'vue-static'
import VueRouter from 'vue-router';

import { library } from '@fortawesome/fontawesome-svg-core'
import { 
  faFolder, 
  faFolderOpen, 
  faFile, 
  faDownload, 
  faRedo,
  faWindowClose,
  faBackward,
  faForward,
  faFastBackward,
  faFastForward,
  faPause,
  faChevronCircleUp
} from '@fortawesome/free-solid-svg-icons'
import { FontAwesomeIcon } from '@fortawesome/vue-fontawesome'
 
library.add(faFolder);
library.add(faFolderOpen);
library.add(faFile);
library.add(faDownload);
library.add(faRedo);
library.add(faWindowClose);
library.add(faBackward);
library.add(faForward);
library.add(faFastBackward);
library.add(faFastForward);
library.add(faPause);
library.add(faChevronCircleUp);

Vue.component('font-awesome-icon', FontAwesomeIcon)
Vue.config.productionTip = false

Vue.use(VueRouter);
Vue.use(VueStatic);


const routes = [
  { path: '/', name: 'Simulation', component: () => import('./pages/Simulation.vue') },
  { path: '/about', name: 'About', component: () => import('./pages/About.vue') }
]
const router = new VueRouter({
  mode: 'history',
  routes 
})

new Vue({
  router,
  render: h => h(App),
}).$mount('#app')