# Asteroids visualizations for web #

## GUI
The GUI is located in asteroid_viz/Web/gui and built on vuejs and uses Threejs for visualizations.

To start the front end, navigate to the gui-folder and run `npm i` to install the required dependencies. When the installation is done, run `npm start`, and the gui will be available at `http://localhost:8080`.

Changes made to the gui do not require a restart. 

For detailed Vue docs see: https://vuejs.org/v2/guide/
For detailed Threejs docs see: https://threejs.org/docs/


### Deploying to staging
The changes done on the web branch will be pushed automatically to the staging environment on Heroku once you make a pull request. You can follow the build process in the pipeline on Bitbucket.

When the build is done, the changes will be visible at 

https://asterix-sim-staging.herokuapp.com

### Deploying to production

To deploy the changes to production you need to have the heroku-cli installed (https://devcenter.heroku.com/articles/heroku-cli).

Once installed, set git remote for heroku to asterix-sim:

```heroku git:remote -a asterix-sim``` 

This is the production environment and should only be deployed from master. To push the changes, make sure you have the last version of master locally. Then run 

```git push heroku master:main``` 

This will push the master branch to heroku's main branch and automatically start a deployment. You will see the build log in your terminal.

When the build is done, the changes will be visible at 

https://asterix-sim.herokuapp.com

NOTE: Only push changes that have been tested in the staging environment. Do not merge pull requests where the staging deployment fails or shows unexpected behavior.

## Server 
The web server is located in AsteroidsMB and is built with python & flask. 

To start the server, navigate to AsteroidsMB and run `python3 -m pip install -r requirements.txt`

This will install all the necessary dependencies you need to run the server. When the installation is done, run `heroku local` and the server will be available at `http://localhost:5000`.

If you don't have the heroku-cli installed, you can run `python3 src/web/sim_api.py` instead.

### End points for the server

#### **GET/POST** `/simulation`
**Params** 
`simulation=configFileName`

Creates the simulation data from the config file and puts it in a numbered run folder in the database.

This magic happens in [sim_runner_core.py](https://bitbucket.org/whoisthegingerbreadman/asteroids/src/web/AsteroidsMB/src/sim_runner_core.py)


#### **GET** `/get-run-dir` 
**Params**  
`run=runFolderTitle`

Returns all objects from specific run. 

**Example response**
```
{
  "run1": [
    {
      "name": "Earth",
      "type": "PointMass",
      "physics": {
      "mass": 20000000000
    },
    "scale": {
      "x": 1,
      "y": 1,
      "z": 1,
      "local": 1
    },
    "positions": [
      {
        "x": 0.2,
        "y": 0,
        "z": 0
      },
      ...
  ]
}
```

#### **GET** `/get-run-dir-structure`
**Params** None

Returns the directory structure of the run folders in the database
```
{
  "folders": [
  {
    "name": string,
    "files": array of strings
  }
}
```

#### **GET** `/file`
**Params**  
`run=runFolderTitle` *required*  
`file=filename` *required*

Returns requested file.