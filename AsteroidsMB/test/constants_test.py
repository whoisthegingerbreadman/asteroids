from src.constants import G


def test_gravitational_constant():
    print("Checking Gravitational Constant")
    assert G == 6.67e-11
