from src.sim_runner_core import run_simulation
import os
import shutil


def test_simple_run():
    cur_dir_name = os.path.dirname(os.path.abspath(__file__))
    test_dir = os.path.join(cur_dir_name, 'test')
    run_simulation(os.path.join(cur_dir_name, "Sims",
                                "pointMass_test.json"),
                   run_directory=test_dir)
    shutil.rmtree(test_dir)
