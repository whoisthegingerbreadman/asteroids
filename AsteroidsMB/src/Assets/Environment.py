"""
Aviv Elbag --- Environment --- September 20th 2020

"""

# IMPORTS
from src.constants import G, dt
import numpy as np
from scipy.spatial import distance
from itertools import combinations
from sklearn.preprocessing import normalize


# OBJECTS
def getDistMat2(num_bodies, bodies):
    position_vecs = np.array(
        [bodies[body].physics.CoM for body in bodies]
    )
    distMat2 = distance.cdist(position_vecs, position_vecs) ** 2

    unitDiffs = np.zeros((num_bodies, num_bodies, 3))
    for combi, combj in combinations(bodies, 2):
        unitDiffs[combi, combj] = (position_vecs[combi] -
                                   position_vecs[combj])
        unitDiffs[combj, combi] = -1 * unitDiffs[combi, combj]
    unitDiffs = np.array([normalize(unitDiff)
                          for unitDiff in unitDiffs])
    return distMat2, unitDiffs


def getDistMat2Alt(position_vecs):
    num_bodies = len(position_vecs)
    position_vecs = np.array(position_vecs)
    distMat2 = distance.cdist(position_vecs, position_vecs) ** 2

    unitDiffs = np.zeros((num_bodies, num_bodies, 3))
    for combi, combj in combinations(np.arange(num_bodies), 2):
        unitDiffs[combi, combj] = (position_vecs[combi] -
                                   position_vecs[combj])
        unitDiffs[combj, combi] = -1 * unitDiffs[combi, combj]
    unitDiffs = np.array([normalize(unitDiff)
                          for unitDiff in unitDiffs])
    return distMat2, unitDiffs


def getMassMat(num_bodies, bodies):
    massVec = np.array([bodies[body].physics.mass for body
                        in bodies])
    massMat = G * np.outer(massVec, massVec)
    for i in range(num_bodies):
        massMat[i, i] = 0
    return massMat


def getForceVectors(massMat, distMat2, unitDiffs):
    forces = np.divide(
        massMat,
        distMat2,
        out=np.zeros_like(distMat2),
        where=distMat2 != 0,
    )
    # print(forces)
    force_vectors = np.sum(np.expand_dims(
        forces, -1) * unitDiffs, axis=0)
    return force_vectors


class Environment:
    def __init__(self, dt=0.01, static_masses=True):
        self.bodies = {}
        self.numBodies = 0
        self.dt = dt
        self.massMat = np.array([])
        self.distMat2 = np.array([])
        self.vecs = np.array([])
        self.step = 0

    def _addBody(self, body):
        self.bodies[self.numBodies] = body
        self.numBodies += 1

    def _updateMassMat(self):
        self.massMat = getMassMat(self.numBodies, self.bodies)

    def _updateDistMat(self):
        self.distMat2, self.unitDiffs = getDistMat2(
            self.numBodies, self.bodies)

    def addBodies(self, bodies):
        for body in bodies:
            self._addBody(body)
        self._updateMassMat()
        self._updateDistMat()
        self.combinations = combinations(range(self.numBodies), 2)

    def Update(self, style="forward_euler"):
        if style == "forward_euler":
            force_vectors = getForceVectors(
                self.massMat, self.distMat2, self.unitDiffs)
            update = Update(force_vectors, self.bodies, dt=self.dt)
            for body in self.bodies:
                self.bodies[body].Update(update, dt=self.dt)
            self._updateDistMat()
            self.step += 1
        elif style == "RK2":
            force_vectors = getForceVectors(
                self.massMat, self.distMat2, self.unitDiffs)
            update = Update(force_vectors, self.bodies, dt=self.dt)
            data_for_bodies = {}
            position_vecs = []
            for body in self.bodies:
                coordinate, velocity, rotation, vrotation = self.bodies[body].Update(update, dt=self.dt/2, set_state=False)  # noqa: E501
                position_vecs.append(coordinate)
                data_for_bodies[body] = {
                    "c": coordinate,
                    "v": velocity,
                    "r": rotation,
                    "vr": vrotation
                }
            distMat2, unitDiffs = getDistMat2Alt(position_vecs)
            force_vectors = getForceVectors(
                self.massMat, distMat2, unitDiffs)
            update = Update(force_vectors, self.bodies, dt=self.dt)
            for body in self.bodies:
                self.bodies[body].Update(update, dt=self.dt)
            self._updateDistMat()
            self.step += 1


class Update:
    def __init__(self, forces, bodies, dt=dt):
        self.dt = dt
        self.dt2 = dt**2
        self.data = {}
        for force, body in zip(forces, bodies):
            name = bodies[body].name
            mass = bodies[body].physics.mass
            self.data[name] = {"f": force, "m": mass}

    def __getitem__(self, key):
        return self.data[key]
