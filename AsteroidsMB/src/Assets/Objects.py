from src.constants import dt
from src.Assets.Properties.Transform import Transform
from src.Assets.Properties.Physics import Physics

import numpy as np


class BaseObject:
    def __init__(self, name=None):
        self.name = name
        self.type = None
        self.hist = []

    @property
    def CoM(self):
        return np.zeros(3)

    def Update(self, update):
        pass

    def as_dict(self):
        pass


class Container(BaseObject):
    """
    A container is an object with many objects with it's own associated
    reference frame.
    """

    def __init__(self, name=None):
        self.name = name
        self.type = "Container"
        self.transform = Transform()
        self.children = []
        self.hist = []
        self.physics = Physics(mass=0)

    @property
    def CoM(self):
        CoM = np.zeros(3)

        for child in self.children:
            CoM += child.CoM * child.physics.mass
        self.physics.CoM = CoM / self.physics.mass
        return self.physics.CoM

    def add_child(self, child):
        self.children.append(child)
        self.physics.update(child)

    def Update(self, update):
        self.transform.Update()
        self.hist.append(self.transform.coordinate + self.CoM)
        direction, magnitude = self.transform.rotation_direction_and_magnitude
        magnitude = magnitude * dt
        for child in self.children:
            child.rotate_around_axis(direction, magnitude, self.CoM)

    def as_dict(self):
        out = {
            "name": self.name,
            "type": self.type,
            "physics": {"mass": self.physics.mass},
            "scale": {
                "x": self.transform.scaleX,
                "y": self.transform.scaleY,
                "z": self.transform.scaleZ,
                "local": self.transform.localScale,
            },
            "children": [child.as_dict() for child in self.children],
        }
        return out


class PointMass(BaseObject):
    """
    PointMass is an asset in either the global or local container.
    """

    def __init__(self, name=None, mass=None, localScale=None):
        self.name = name
        self.type = "PointMass"
        self.physics = Physics(mass=mass)
        self.transform = Transform(localScale=localScale)
        self.hist = []

    @property
    def CoM(self):
        return self.transform.coordinate

    def Update(self, update, dt=dt, set_state=True):
        objectUpdate = update[self.name]
        if set_state:
            self.transform.Update(objectUpdate, set_state=True, dt=dt)
            self.physics.CoM = self.transform.coordinate
            # update_rotation = {self.name:self.transform.rotation}
            self.hist.append(self.transform.coordinate)
            # return  update_coordinate#,self.transform.rotation (add later)
        else:
            return self.transform.Update(objectUpdate, dt=dt/2, set_state=False)  # noqa: E501

    def rotate_around_axis(self, direction, magnitude, CoM):
        self.transform.rotate_around_axis(direction, magnitude, CoM)
        self.hist.append(
            self.transform.coordinate
        )  # perhaps remove this later when this is integrated into update

    def as_dict(self):
        out = {
            "name": self.name,
            "type": self.type,
            "physics": {"mass": self.physics.mass},
            "scale": {
                "x": self.transform.scaleX,
                "y": self.transform.scaleY,
                "z": self.transform.scaleZ,
                "local": self.transform.localScale,
            },
        }
        return out
