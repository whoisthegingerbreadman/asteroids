from src.constants import dt
import numpy as np


class Transform:
    def __init__(
        self,
        x=0,
        y=0,
        z=0,
        vx=0,
        vy=0,
        vz=0,
        rx=0,
        ry=0,
        rz=0,
        vrx=0,
        vry=0,
        vrz=0,
        localScale=1,
        scaleX=1,
        scaleY=1,
        scaleZ=1,
    ):
        self.x, self.y, self.z = (x, y, z)  # position
        self.vx, self.vy, self.vz = (vx, vy, vz)  # translational velocity
        self.rx, self.ry, self.rz = (rx, ry, rz)  # rotation
        self.vrx, self.vry, self.vrz = (vrx, vry, vrz)  # rotational velocity
        self.localScale, self.scaleX, self.scaleY, self.scaleZ = (
            localScale,
            scaleX,
            scaleY,
            scaleZ,
        )  # scale in current context

    def updateScale(self, scale):
        self.localScale = scale

    def Update(self, update=None, dt=dt, set_state=True):
        if update is not None:
            acceleration = update["f"] / update["m"]
            coordinate = self._update_position(*acceleration, dt=dt)
            velocity = self._update_velocity(*acceleration, dt=dt)
            rotation = self._update_rotation(
                0, 0, 0, dt=dt
            )
            # update later... Usually doesn't matter for point masses.
            velocity_rotation = self._update_velocity_rotation(0, 0, 0, dt=dt)
        else:
            coordinate = self._update_position(
                0, 0, 0
            )  # Update the zeros later... Will be much more complicated.
            # We are assuming containers are much much bigger than satellites.
            velocity = self._update_velocity(0, 0, 0, dt=dt)
            rotation = self._update_rotation(0, 0, 0, dt=dt)
            velocity_rotation = self._update_velocity_rotation(0, 0, 0, dt=dt)

        if set_state:
            self.coordinate = coordinate
            self.velocity = velocity
            self.rotation = rotation
            self.velocity_rotation = velocity_rotation
        else:
            return coordinate, velocity, rotation, velocity_rotation

    def _update_position(self, ax, ay, az, coord=None, velocity=None, dt=dt):

        if coord is None:
            x, y, z = self.coordinate
        else:
            x, y, z = coord

        if velocity is None:
            vx, vy, vz = self.velocity
        else:
            vx, vy, vz = velocity

        x += vx * dt + 0.5 * ax * dt**2
        y += vy * dt + 0.5 * ay * dt**2
        z += vz * dt + 0.5 * az * dt**2
        return x, y, z

    def _update_velocity(self, ax, ay, az, velocity=None, dt=dt):
        if velocity is None:
            vx, vy, vz = self.velocity
        else:
            vx, vy, vz = velocity

        vx += ax * dt
        vy += ay * dt
        vz += az * dt
        return vx, vy, vz

    def _update_rotation(self, arx, ary, arz, rotation=None, vrotation=None, dt=dt):  # noqa: E501
        if rotation is None:
            rx, ry, rz = self.rotation
        else:
            x, y, z = rotation

        if vrotation is None:
            vrx, vry, vrz = self.velocity_rotation
        else:
            vrx, vry, vrz = vrotation

        rx = rx + vrx * dt + 0.5 * arx * dt ** 2
        ry = ry + vry * dt + 0.5 * arx * dt ** 2
        rz = rz + vrz * dt + 0.5 * arx * dt ** 2

        return rx, ry, rz

    def _update_velocity_rotation(self, arx, ary, arz, vrotation=None, dt=dt):
        if vrotation is None:
            vrx, vry, vrz = self.velocity_rotation
        else:
            vrx, vry, vrz = vrotation

        vrx += arx * dt
        vry += ary * dt
        vrz += arz * dt
        return vrx, vry, vrz

    def rotate_around_axis(self, unit_axis, alpha, center=np.zeros(3)):
        x = self.coordinate - center
        # print(self.coordinate)
        # print(x)
        x = (
            (x - unit_axis * np.dot(x, unit_axis)) * np.cos(alpha)
            + np.cross(x, unit_axis) * np.sin(alpha)
            + x * np.dot(x, unit_axis)
        )
        # print(alpha)
        # print(x)
        self.coordinate = x + center
        # print(self.coordinate)

    @property
    def coordinate(self):
        return np.array([self.x, self.y, self.z])

    @property
    def velocity(self):
        return np.array([self.vx, self.vy, self.vz])

    @property
    def rotation(self):
        return np.array([self.rx, self.ry, self.rz])

    @property
    def velocity_rotation(self):
        return np.array([self.vrx, self.vry, self.vrz])

    @property
    def scale(self):
        return np.array([self.localScale,
                         self.scaleX,
                         self.scaleY,
                         self.scaleZ])

    @coordinate.setter
    def coordinate(self, coordinate):
        self.x, self.y, self.z = coordinate

    @velocity.setter
    def velocity(self, velocity):
        self.vx, self.vy, self.vz = velocity

    @rotation.setter
    def rotation(self, rotation):
        self.rx, self.ry, self.rz = rotation

    @velocity_rotation.setter
    def velocity_rotation(self, velocity_rotation):
        self.vrx, self.vry, self.vrz = velocity_rotation

    @scale.setter
    def scale(self, scale):
        self.localScale, self.scaleX, self.scaleY, self.scaleZ = scale

    @property
    def rotation_direction_and_magnitude(self):
        magnitude = np.linalg.norm(self.velocity_rotation)
        direction = self.velocity_rotation / magnitude
        return direction, magnitude


if __name__ == "__main__":
    T = Transform()
    print(T.coordinate)
