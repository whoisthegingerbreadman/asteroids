import numpy as np


class Physics:
    def __init__(self, mass=1, CoM=np.zeros(3)):
        self.mass = mass
        self.CoM = CoM

    def update(self, child):
        self.mass += child.physics.mass
        self.CoM += child.physics.mass * child.physics.CoM / self.mass
        print(self.CoM)
