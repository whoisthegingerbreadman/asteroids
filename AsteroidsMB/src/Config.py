"""
Aviv Elbag --- Read Asteroid Configs --- September 20th 2020
"""
# IMPORTS

import json

from src.Assets.Objects import PointMass, Container
from src.Assets.Properties.Transform import Transform
from src.Assets.Properties.Physics import Physics

# PRIVATE METHODS


def _read_transform(transform):
    x = transform["position"]["x"]
    y = transform["position"]["y"]
    z = transform["position"]["z"]
    coordinate = (x, y, z)
    vx = transform["velocity"]["x"]
    vy = transform["velocity"]["y"]
    vz = transform["velocity"]["z"]
    velocity = (vx, vy, vz)
    rx = transform["rotation"]["x"]
    ry = transform["rotation"]["y"]
    rz = transform["rotation"]["z"]
    rotation = (rx, ry, rz)
    vrx = transform["rotation_velocity"]["x"]
    vry = transform["rotation_velocity"]["y"]
    vrz = transform["rotation_velocity"]["z"]
    rotation_velocity = (vrx, vry, vrz)
    sl = transform["scale"]["local"]
    sx = transform["scale"]["x"]
    sy = transform["scale"]["y"]
    sz = transform["scale"]["z"]
    scale = (sl, sx, sy, sz)
    return coordinate, velocity, rotation, rotation_velocity, scale


def _make_container(cd):
    container = Container(name=cd["name"])
    coordinate, velocity, rotation, velocity_rotation, scale = _read_transform(
        cd["transform"]
    )
    container.transform.coordinate = coordinate
    container.transform.velocity = velocity
    container.transform.rotation = rotation
    container.transform.velocity_rotation = velocity_rotation
    container.transform.scale = scale
    for child in cd["children"]:
        processed_child = make_object(child)
        container.add_child(processed_child)
    return container


def _make_body(body_spec):
    body = PointMass()
    body.name = body_spec["name"]

    transform = Transform()

    coordinate, velocity, _, _, scale = _read_transform(body_spec["transform"])

    transform.coordinate = coordinate
    transform.velocity = velocity
    transform.scale = scale

    body.transform = transform

    physics = Physics()
    physics.mass = body_spec["physics"]["mass"]
    physics.CoM = coordinate

    body.physics = physics

    return body


# PUBLIC METHODS


def make_object(obj):
    # print(obj)
    if obj["type"] == "container":
        obj = _make_container(obj)
    elif obj["type"] == "point_mass":
        obj = _make_body(obj)
    return obj


# CLASSES


class Config:
    def __init__(self):
        self.numBodies = 0
        self.bodies = []
        self.numSteps = 0

    def readConfig(self, config_loc):
        with open(config_loc, "r") as f:
            config = json.load(f)
        self.numSteps = config["steps"]
        self.dt = config["dt"]
        for body_spec in config["bodies"]:
            # print(body_spec)
            body = make_object(body_spec)
            self.bodies.append(body)
