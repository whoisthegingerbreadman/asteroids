from flask import Flask
from flask import request, jsonify
from flask_cors import CORS
import json


import os

import sys  # noqa
sys.path.append(os.path.abspath(os.path.join(os.path.dirname(os.path.abspath(__file__)), '..', '..')))  # noqa
from src.sim_runner_core import run_simulation, get_run_histories, get_run_history  # noqa: E402 F401 E501

app = Flask(__name__)
CORS(app)
app.config["DEBUG"] = True

base_path = os.path.abspath(os.path.join(
    os.path.abspath(__file__), "..", "..", ".."))
sims_path = os.path.join(base_path, "src", "Sims")
runs_path = os.path.join(base_path, "runs")


@app.route('/', methods=["GET"])
def home():
    return '<h1>Welcome to Asterix!</h1>'


@app.route("/simulation", methods=["GET", "POST"])
def simulate():
    if request.method == "GET":
        sim_id = request.args.getlist("simulation")[0]
        histories = run_simulation(
            os.path.join(sims_path, sim_id + '.json'), style="RK2",
            from_api=True)

        return jsonify(histories)
    # return histories
    if request.method == "POST":
        pass
        # json.dump(blah-blah)


@app.route("/file", methods=["GET", "POST"])
def run():
    if request.method == "GET":
        print("OK2")
        run_id = request.args.getlist("run")[0]
        file_id = request.args.getlist("file")[0]
        file_out_ext = os.path.splitext(file_id)[1]
        print(file_out_ext)
        file_path = os.path.join(runs_path, run_id, file_id)
        if file_out_ext == ".csv":
            file_out = get_run_history(file_path)
        elif file_out_ext == ".json":
            with open(file_path, 'r') as f:
                file_out = json.load(f)
        return jsonify(file_out)
        # return jsonify(file_out)
    # return histories
    if request.method == "POST":
        pass


@app.route("/get-run-dir", methods=["GET"])
def get_run_dir():
    all_objects = []
    if request.method == "GET":
        run_name = request.args.getlist("run")[0]
        for f in os.listdir(os.path.join(runs_path, run_name)):
            all_objects.append(f)
    return jsonify(all_objects)


@app.route("/get-run-dir-structure", methods=["GET"])
def get_run_dir_structure():
    all_runs = []
    if request.method == "GET":

        for run_name in os.listdir(runs_path):
            new_object = {'name': run_name, 'files': []}
            for f in os.listdir(os.path.join(runs_path, run_name)):
                new_object['files'].append(f)
            all_runs.append(new_object)
        return jsonify(all_runs)
