"""
Aviv Elbag --- MAIN SIM RUNNER --- September 20th 2020

This is the entry point for the Asteroid simulator.
Uses the initialization simulations from the AsteroidsMB/Sims folder.
This script will output a run folder which will include a set of history and
body files that describe the motions of the body as well as their
geometric characteristics.

Example run from the command line:

python sim_runner_core.py Sims/first_simulation.json
"""
# IMPORTS
from tqdm import tqdm
import logging  # noqa: F401
import os
import sys
from shutil import copyfile
sys.path.append(os.path.abspath(os.path.join(
    os.path.dirname(os.path.abspath(__file__)), '..')))

import json  # noqa: E402
import time  # noqa: E402, F401
import glob  # noqa: E402
import numpy as np  # noqa: E402

from src.Assets.Environment import Environment  # noqa: E402
from src.Config import Config  # noqa: E402


# PRIVATE METHODS


def _make_coordinate_history(
    body, parent=None, base_path=".", coordinate_output_file="history"
):
    path = os.path.join(base_path, coordinate_output_file +
                        str(body.name) + ".csv")
    if parent is not None:
        coordinates = np.array(parent.hist) + np.array(body.hist)
    else:
        coordinates = np.array(body.hist)
    np.savetxt(path, coordinates, delimiter=",")


def _make_body_metadata_history(
    body, parent=None, base_path=".", body_output_file="body"
):
    if parent is not None:
        filename = body_output_file + parent.name + "-" + body.name
    else:
        filename = body_output_file + body.name
    path = os.path.join(base_path, filename + ".json")
    with open(path, "w") as f:
        json.dump(body.as_dict(), f)


def _make_body_names(all_bodies, run_directory):
    with open(os.path.join(run_directory, "body_names.txt"), "w") as f:
        f.writelines(all_bodies)


def _copy_simulation_file(simulation_file, run_directory):
    dst = os.path.join(run_directory, "simulation_definition.json")
    copyfile(simulation_file, dst)
# PUBLIC METHODS


def get_run_histories(run_path):
    histories = {}
    history_files = []
    for f_str in os.listdir(run_path):
        if os.path.splitext(f_str)[-1] == ".csv":
            history_files.append(f_str)
    for i, history in enumerate(sorted(history_files)):
        cur_path = os.path.join(run_path, history)
        # print(cur_path)
        data = get_run_history(cur_path)
        histories[history[7:-4]] = data
    return histories


def get_run_history(history_path):
    data = np.loadtxt(history_path, delimiter=",").tolist()
    positions = []
    for elem in data:
        positions.append({'x': elem[0], 'y': elem[1], 'z': elem[2]})
    return positions


def output_to_files(simEnv,
                    coordinate_output_file="history",
                    body_output_file="body",
                    simulation_file=None, run_directory=None):
    if run_directory is None:
        cur_dir = os.path.abspath(os.path.join(
            os.path.abspath(__file__), "..", ".."))
        run_base_dir = os.path.join(cur_dir, "runs")
        run_number = len(glob.glob(run_base_dir+"/run*")) + 1
        run_directory = os.path.join(run_base_dir, "run" + str(run_number))
    os.mkdir(run_directory)
    all_focus_bodies = []
    for body in simEnv.bodies:
        focus_body = simEnv.bodies[body]
        if focus_body.type == "Container":
            for children in focus_body.children:
                _make_coordinate_history(
                    children,
                    parent=focus_body,
                    base_path=run_directory,
                    coordinate_output_file=coordinate_output_file,
                )
                _make_body_metadata_history(
                    children,
                    parent=focus_body,
                    base_path=run_directory,
                    body_output_file=body_output_file,
                )
                all_focus_bodies.append(focus_body.name)
        else:
            _make_coordinate_history(
                focus_body,
                base_path=run_directory,
                coordinate_output_file=coordinate_output_file,
            )
            _make_body_metadata_history(
                focus_body, base_path=run_directory,
                body_output_file=body_output_file
            )
            all_focus_bodies.append(focus_body.name + "\n")
    _make_body_names(all_focus_bodies, run_directory)
    if simulation_file is not None:
        _copy_simulation_file(simulation_file, run_directory)
    return os.path.abspath(run_directory)


def run_simulation(input_file, run_directory=None, style="forward_euler",
                   from_api=False):
    # logging.basicConfig("log.txt")

    config = Config()
    config.readConfig(input_file)
    simEnv = Environment(dt=config.dt)
    simEnv.addBodies(config.bodies)
    for i in tqdm(range(config.numSteps)):
        simEnv.Update(style=style)
    run_path = output_to_files(simEnv, simulation_file=input_file,
                               run_directory=run_directory)

    if from_api:
        histories = get_run_histories(run_path)
        return histories


# COMMAND LINE INTERFACE

if __name__ == "__main__":
    import sys
    if len(sys.argv) < 3:
        style = None
    else:
        style = sys.argv[2]
    print(style)
    run_simulation(sys.argv[1], style=style)
