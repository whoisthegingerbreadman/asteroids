# Where we will keep all the physical constants.

G = 6.67e-11  # Gravitational Constant
dt = 0.1  # default time step for the propagator
dt2 = dt ** 2  # squared time step for propagator
