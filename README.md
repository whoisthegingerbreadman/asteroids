![Logo](README-images/asteroid-logo.png)

# Asteroids README #

AsteroidMB stands for Asteroid Multibody. This software was made to simulate asteroids and bodies moving around gravitating bodies
in a realistic force-centric way. 

Running your first simulation is simple. All you need to do is, from the AsteroidsMB folder, run 

```bash
python src/sim_runner_core.py src/Sims/first_simulation.json
```
### How to run? 

Although it is useful to be able to run the simulator independently what most people would really like to do is play around with the front facing interface. To do so 

1. navigate into asteroid_viz/Web/gui 

2. run `npm install`

3. run `npm start`

4. open another terminal

5. navigate into asteroid_viz/Web/server

6. run `npm install`

7. run `npm start`

8. In your browser go to localhost:8080

9. Voila! 

![Logo](README-images/show-off1.gif)
### What is the idea? ###

Asteroids is initially looking at developing software that will prove the feasibility of using small sats to do gravimetry on small bodies. This kind of analysis will help back out the mass distribution of small bodies using imperfect and noisy time-of-flight measurements of a number of satellites that are released nearby the body. 

In the far-field a nonuniform body's gravity very nearly approximates that of a point mass. From this far field gravity it is very difficult to discern a body's irregularities. However, as you near the object the gravity topology of the object becomes less spherical and less uniform. It can be shown that a body's gravity is one-to-one with a mass distribution and as a result knowing the gravity topology is the same as knowing the mass distribution itself. 

![Logo](README-images/gravity-field1.png)

Knowing a body's mass distribution is very useful in prospecting of said bodies as well as finding very low orbits for these bodies.

There are many near earth objects(NEOs) and this fact is the main point of product. In the near future it shouldn't be too difficult to stop by one and start manufacturing on the body itself.

![Logo](README-images/near-earth-asteroid-map.gif)

In the future we will also develop cheap small satellites that will be the satellites in question. For now, however, we will make simulations. :) This is an exampl of such a satellite.


![Logo](README-images/KickSprite_sm.jpg)

### Why is this useful? ###

Asteroids are unimaginably valuable. Not only because some asteroids are rich in precious metals such as Platinum and Gold but also because the massive amount of material that is already in space needs no push to escape Earth's gravity. The most valuable of these asteroids is estimated to be valued at 15 quintillion dollars. However, the near Earth objects(NEO) are (luckily) much more modest. 

| Asteroid Name | Mass(kg) | Est. Value (Billions $) | Est. Profit (Billions $) | Composition |
|-------------|--------------|--------------|--------------|--------------|
| Ryugu       | 4.5e11       | 83           | 30           | Nickel, iron, cobalt, water, nitrogen, hydrogen, ammonia |
| 1989 ML     | ?            | 14           | 4            | Nickel, iron, cobalt                                     |
| Nereus      | ?            | 5            | 1            | Nickel, iron, cobalt                                     |
| Bennu       | 7.3e10       | .7           | .2           | Iron, hydrogen, ammonia, nitrogen                        |
| Didymos     | ?            | 62           | 16           | Nickel, iron, cobalt                                     |
| 2011 UW158  | ?            | 7            | 2            | Platinum, nickel, iron, cobalt                           |
| Anteros     | 1.5e13       | 5570         | 1250         | Magnesium silicate, aluminum, iron silicate              |
| 2001 CC21   | ?            | 147          | 30           | Magnesium silicate, aluminum, iron silicate              |
| 1992 TC     | ?            | 84           | 17           | Nickel, iron, cobalt                                     |
| 2001 SG10   | ?            | 3            | .5           | Nickel, iron, cobalt                                     |
| Psyche      | 24e19?       | 27.67        | 1.78         | Nickel, iron, cobalt, gold                               |

On a Falcon 9 the cost of raising a single kilogram to LEO is on the order of $2000. That means the cost of lifting Ryugu to LEO is on the order of 90 trillion dollars. In comparison the global GDP of the world is 80 trillion dollars. In effect, asteroids are a shortcut to developing in-space infrastructure. 

### How are simulations structured? ###

Simulations take the inertial reference frame perspective. Every object lives under a transform and have physics. 
The update cascades through all the objects. 

### Contribution guidelines ###

We follow standard git practices. Although, for now, we will be relatively light on the requirements. 

* Every contribution should follow PEP guidelines for python. 
    - To follow this guidline we suggest you use flake8 to lint your code.
* Every contribution should have testing that covers the contribution.
* Every contribution must be reviewed by at least one other knowledgeable member of Asteroids.
* Every contribution must satisfy the unit testing up to the point that it was contributed. 

### Main pain points right now ###

Being a developing project there is much to do and the list we have here is by no means exhaustive.
Some things we have in mind are:

* Better renderings of asteroids using Unity, Godot, or three.js. Although we prefer three.js as we see this being the main rendering software moving forwards.
* Higher fidelity physics modeling. Right now we use a simple Euclidean style update which leaves a lot to be desired. Implementing leapfrog integration or whatever other that increases the fidelity of the model would be very useful. 
* Better testing that more accurately represents the fidelity of the model. 
* Better interactivity that allows to generate initialization configs.(This is a mix of three js and the backend)
* Symplectic Integrator for energy conservation
* Interface that allows for running simulations from the front end
* Deploy out to the world 
* Set up continuous deployment 
* Improve aesthetic of the front-end 
* Better looking asteroids 
* Implement communicating satellites with noise
* SLAM for gravity map/ mass distribution
* Testing for fidelity of simulation
* Rewrite simulation python calls into C++ and wrap into python(with SWIG or something else)
* Implement a database input instead of file making simulator side.
* Code coverage tester of simulator
* integrate node tests into pipelines
