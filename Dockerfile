FROM python:3.6

WORKDIR /asteroids

COPY requirements.txt requirements.txt

COPY . /asteroids

RUN python3 -m pip install -r requirements.txt


#RUN python AsteroidsMB/src/web/sim_api.py

EXPOSE 8080

CMD ["python3", "AsteroidsMB/src/web/sim_api.py"]